import { createSlice } from '@reduxjs/toolkit'
const initialState = {
    tasks: []
}
const taskSlice = createSlice({
    name: 'task',
    initialState,
    reducers: {
        fetchTaskAction: (state, action) => {
            return {
                ...state.tasks,
                tasks: action.payload
            }
        },
        deleteTaskAction: (state, action) => {
            return {
                ...state,
                tasks: state.tasks.filter((task) => task.id !== action.payload)
            }
        },
        createTaskAction: (state, action) => {
            return {
                ...state,
                tasks: [...state.tasks, action.payload]

            }
        },
        updateTaskAction: (state, action) => {
            state.tasks = state.tasks.map((task) => task.id === action.payload.id ? action.payload : task)
        },
        checkboxTaskTodo: (state, action) => {
            state.tasks = state.tasks.map(task => {
                if (task.id === action.payload) {
                    return {
                        ...task,
                        isCompleted: !task.isCompleted
                    }
                }
                return task
            })
        },
        unCheckTaskTodo: (state, action) => {
            state.tasks = state.tasks.map((task) => {
                if (task.id === action.payload) {
                    return {
                        ...task,
                        isCompleted: !task.isCompleted
                    }
                }
                return task
            })
        }

    }
})
export const { fetchTaskAction, deleteTaskAction, createTaskAction, updateTaskAction, checkboxTaskTodo, unCheckTaskTodo } = taskSlice.actions
export default taskSlice.reducer