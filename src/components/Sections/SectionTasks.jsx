import React, {  useState } from 'react'
import { useDispatch } from 'react-redux';
import { deleteTask, createSectionTask, completeTask } from '../../Api/Api'
import { deleteTaskAction, createTaskAction, checkboxTaskTodo } from '../../Store/Features/TodosSlice'
import { Button, Popover, Checkbox } from 'antd';
import { CalendarOutlined } from '@ant-design/icons';
import Form from '../Tasks/Form';
import { useSnackbar } from 'notistack';
import Taskdata from '../Tasks/TodayTask';
function SectionTasks({ sectionid, projectId, tasks, selectedSectionId }) {
    // console.log('From SectionTasks',selectedSectionId);
    const { enqueueSnackbar } = useSnackbar();
    const [isformopen, setisformopen] = useState(false)
    const toggleform = () => {
        setisformopen(!isformopen)
    }

    const dispatch = useDispatch()
    const Delete = async (id) => {
        const response = await deleteTask(id);
        dispatch(deleteTaskAction(id))
    }
    const AddSectiontask = async (name, date, string, description) => {
        try {
            const response = await createSectionTask(name, projectId, date, selectedSectionId, string, description)
            dispatch(createTaskAction(response))
        }
        catch (error) {
            console.log("Error in creating task in section", error)
        }
    }
    const handleChecbox = async (taskId) => {
        try {
            const res = await completeTask(taskId)
            dispatch(checkboxTaskTodo(taskId))
            enqueueSnackbar('Task Completed')
        } catch (error) {
            console.log('Error in checkboxTask', error);
        }
    }
    return (
        <div style={{display:'flex',flexDirection:'column',width:855,marginLeft:'-20px'}}>
            {tasks.filter(task => (!projectId || task.projectId === projectId) && (!sectionid || task.sectionId === sectionid))
                .map((data) => {
                    return (
                        <ul key={data.id} style={{ listStyle: 'none' }}>
                            <li style={{ fontSize: '1.2rem', display: 'flex', justifyContent: 'space-between',marginLeft:'-10px' }}>
                                <div className="div" style={{ display: "flex", flexDirection: 'column' }}>
                                    <span><Checkbox
                                        style={{
                                            paddingRight: '1rem'
                                        }}
                                        onChange={() => {
                                            handleChecbox(data.id)
                                        }}
                                    ></Checkbox>{data.content}</span>
                                    <span style={{ fontSize: '10px', color: 'gray', marginLeft: '2rem' }}>{data.description}</span>
                                    <span style={{ color: 'green', fontSize: '15px', marginLeft: '2rem', marginTop: '5px' }}>
                                        <CalendarOutlined />{data.due?.string}
                                    </span>
                                </div>
                                <div className="buttons" style={{ width: 100, display: 'flex', justifyContent: 'space-between ' }}>
                                    {/* <EditOutlined style={{ cursor: 'pointer' }} onClick={() => { handleedit(data.id); setisformopen(true) }} /> */}
                                    <Popover trigger="click" placement="bottomright"
                                        content={
                                            <Button danger onClick={() => Delete(data.id)}>Delete</Button>
                                        }
                                    >
                                        <Button className='listbtn' >{'...'}</Button>
                                    </Popover>
                                </div>
                            </li>
                        </ul>
                        // <Taskdata  handleChecbox={handleChecbox} data={data}/>
                    )
                })
            }
            <Form title={'Add Task'} handleAdd={AddSectiontask} isformopen={isformopen} toggleform={toggleform} />
        </div>
    )
}

export default SectionTasks
