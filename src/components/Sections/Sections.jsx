import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getsections, deleteSection, updateSectionAction } from '../../Api/Api'
import { fetchSection, deleteSectionAction, addSection, updateSection } from '../../Store/Features/SectionSlice'
import SectionTasks from './SectionTasks'
import { Button } from 'antd'
import { createSection } from '../../Api/Api'
import { LoadingOutlined } from '@ant-design/icons';
import { Spin, Popover } from 'antd';
import { useSnackbar } from 'notistack';
import SectionForm from './SectionForm'
import { deleteTask, createSectionTask, completeTask } from '../../Api/Api'
import { deleteTaskAction, createTaskAction, checkboxTaskTodo } from '../../Store/Features/TodosSlice'

function Sections({ projectId, tasks }) {

    const { enqueueSnackbar } = useSnackbar();
    const [currentProjectId, setCurrentProjectId] = useState(projectId);
    const [selectedSectionId, setSelectedSectionId] = useState(null);
    const [loading, setLoading] = useState(true);
    const [editSectionId, setEditSectionId] = useState(null)
    const [sectionName, setSectionName] = useState('')


    const handleSectionSelect = (sectionId) => {
        setSelectedSectionId(sectionId);
    };

    const dispatch = useDispatch()
    const data = useSelector((state) => state.Section.sections)
    const fetchSections = async () => {
        try {
            const response = await getsections(projectId)
            dispatch(fetchSection(response))
            setLoading(false)
            // enqueueSnackbar("Successfully fetched sections", { variant: 'success' })
        }
        catch (err) {
            enqueueSnackbar('Error in fetching sections', err, {})
        }
    }
    useEffect(() => {
        fetchSections()

    }, [projectId])

    useEffect(() => {
        setCurrentProjectId(projectId);
        // setSelectedId(null);
        setSelectedSectionId(null)
    }, [projectId]);


    const deleteSectionId = async (id) => {
        const response = await deleteSection(id);
        // console.log("From Delete section :", response);
        dispatch(deleteSectionAction(id))
    }

    const AddSection = async (input) => {
        const response = await createSection(input, projectId)
        dispatch(addSection(response))
    }
    const handleEditClick = (id, name) => {
        setSectionName(name)
        setEditSectionId(id)
    }
    const handleUpdateSection = async (id) => {
        // console.log(id, "From update function");
        try {
            const response = await updateSectionAction(id, sectionName);
            dispatch(updateSection(response))
            setEditSectionId(null);
            setSectionName('');
        }
        catch (err) {
            console.log('Error updating section', err);
        }
    }
    const AddSectiontask = async (name, date, string, description) => {
        try {
            const response = await createSectionTask(name, projectId, date, selectedSectionId, string, description)
            dispatch(createTaskAction(response))
        }
        catch (error) {
            console.log("Error in creating task in section", error)
        }
    }
    return (
        <div style={{ display: 'flex', flexDirection: 'column', justifyContent: 'space-between' }}>
            <center><Spin spinning={loading} indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />} /></center>
            <SectionForm title={'Add Section'} handleAdd={AddSection} />
            {data.map((item) => (
                <div key={item.id} style={{ width: '40vw', height: 'auto', margin: '20px', borderRadius: '10px', padding: '10px' }} onClick={() => { handleSectionSelect(item.id); console.log('Selectedsectionid', item.id); }}>
                    {editSectionId === item.id ? (
                        <form onSubmit={(e) => { e.preventDefault(); handleUpdateSection(item.id) }} style={{ display: 'flex', justifyContent: 'space-between', flexDirection: 'column', height: 80 }}>
                            <input value={sectionName} onChange={(e) => setSectionName(e.target.value)} style={{ height: 40, fontSize: '16px' }} />
                            <div style={{ display: 'flex' }}>
                                <Button style={{ backgroundColor: 'red', color: 'white', border: 'none', borderRadius: '5px', fontWeight: 600 }} onClick={() => { handleUpdateSection(item.id); }}>Save</Button>
                                <Button style={{ border: 'none', borderRadius: '5px', fontWeight: 600 }} onClick={() => { setEditSectionId(null); }}>Cancel</Button>
                            </div>
                        </form>
                    ) : (
                        <li style={{ fontSize: '1.2rem', display: 'flex', justifyContent: 'space-between', width: 760 }}>
                            <div className="div" style={{ display: "flex", flexDirection: 'column' }}>
                                <span style={{ fontSize: '20px', fontWeight: 550 }}>{item.name}</span>
                            </div>
                            <Popover trigger="click" style={{ border: 'none' }}
                                content={
                                    <div div style={{ display: 'flex', height: 100, width: 100, justifyContent: 'space-around', flexDirection: 'column' }}>
                                        <Button style={{ border: 'none' }} danger onClick={() => { deleteSectionId(item.id) }}>Delete</Button>
                                        <Button style={{ border: 'none' }} onClick={() => handleEditClick(item.id, item.name)}>Edit</Button>
                                    </div>
                                }
                            >
                                <Button className='listbtn' >{'...'}</Button>
                            </Popover>

                        </li>
                    )}
                    <SectionTasks sectionid={item.id} projectId={projectId} tasks={tasks} selectedSectionId={selectedSectionId} />
                </div>
            ))
            }
            <SectionForm title={'Add Section'} handleAdd={AddSection} />
        </div >
    )
}

export default Sections
