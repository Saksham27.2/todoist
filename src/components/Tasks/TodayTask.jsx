import React,{useState} from 'react'
import { Button, Checkbox, Popover } from 'antd'
import { CalendarOutlined, EditOutlined, TagOutlined, } from '@ant-design/icons';
import { useDispatch } from 'react-redux';
import {deleteTaskAction,unCheckTaskTodo,checkboxTaskTodo} from '../../Store/Features/TodosSlice'
import {deleteTask,unCompleteTask,completeTask} from '../../Api/Api'
import { useSnackbar } from 'notistack';


function Taskdata({ data,  todaydate, handleedit,  setOpenForm, title }) {
    const dispatch=useDispatch()
    const { enqueueSnackbar } = useSnackbar();
    const [completed, setCompleted] = useState(false)
    const deleteTaskId = async (id) => {
        const response = await deleteTask(id);
        dispatch(deleteTaskAction(id))
    }
    const handleUncheckTask = async (taskid) => {
        setCompleted(false)
        try {
            const response = await unCompleteTask(taskid)
            dispatch(unCheckTaskTodo(taskid))
        }
        catch (err) {
            console.log('Error in uncheckTask', err);
        }
    }
    const handleChecbox = async (taskId) => {
        setCompleted(true)
        try {
            const res = await completeTask(taskId)
            console.log("Before updating", completed);
            dispatch(checkboxTaskTodo(taskId))
            console.log(completed, "State of complete in checkbox");
            enqueueSnackbar(<div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center', width: 200 }}><span style={{ fontSize: '15px', fontWeight: 550 }}>1 task completed </span><button className='alertbtn' onClick={() => handleUncheckTask(taskId)}>Undo</button></div>, { variant: 'info' })
        } catch (error) {
            console.log('Error in checkboxTask', error);
        }
    }
    return (
        <ul key={data.id} style={{ listStyle: 'none' }}>
            <li style={{ fontSize: '1.2rem', display: 'flex', justifyContent: 'space-between', width: 770 }}>
                <div key={data.id} className="div" style={{ display: "flex", flexDirection: 'column' }}>
                    <span style={{ display: 'flex', alignItems: 'center' }}>
                        <Checkbox
                            style={{
                                paddingRight: '1rem', marginTop: '-5px'
                            }}
                            onChange={() => {
                                handleChecbox(data.id)
                            }}
                        ></Checkbox>
                        {data.content}</span>
                    <span style={{ fontSize: '13px', color: 'gray', marginTop: '3px', marginBottom: '3px', marginLeft: '2rem' }}>
                        {data.description}
                    </span>
                    <span key={data.id} style={{ color: 'green', fontSize: '15px', width: 130, display: 'flex', justifyContent: 'space-between', marginLeft: '2rem' }} >
                        {title === 'todaydata' ? '' : <span><CalendarOutlined />{data.due?.string === todaydate ? 'Today' : data.due?.string}</span>}
                        {data.labels.map((label) => {
                            return (<span key={label.id} style={{ fontSize: '14px', color: 'slategrey' }}><TagOutlined style={{ marginRight: 3 }} />{label}</span>)
                        })
                        }
                    </span>
                </div>
                <div className="buttons" style={{ width: 100, display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
                    <EditOutlined style={{ cursor: 'pointer' }} onClick={() => { handleedit(data.id); setOpenForm(true) }} />
                    <Popover placement="bottomleft" trigger="click"content={<Button danger onClick={() => deleteTaskId(data.id)}>Delete</Button>}>
                        <Button className='listbtn' >{'...'}</Button>
                    </Popover>
                </div>
            </li>   
        </ul>
    )
}

export default Taskdata
