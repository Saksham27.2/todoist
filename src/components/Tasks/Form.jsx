import React, { useState, useEffect } from 'react';
import { ConfigProvider, DatePicker, Space, Button, Form, Select, Input } from 'antd';
import { PlusOutlined } from '@ant-design/icons'
import { useDispatch, useSelector } from 'react-redux';
import './task.css'
import { updateTaskapi, createTask } from '../../Api/Api'
import { updateTaskAction, createTaskAction } from '../../Store/Features/TodosSlice'





const TaskForm = ({ title, editmode, isformopen, todaydate, toggleform }) => {
  const dispatch = useDispatch()
  const [selecteddate, setselecteddate] = useState(null)
  const [selectedstring, setselectedstring] = useState('')
  const [input, setinput] = useState('')
  const [description, setDescription] = useState('')
  const Labeldata = useSelector((state) => state.Label.Labels)
  const projectnames = useSelector(state => state.projects.Projects)
  const projidinbox = '2331888192'
  const [selectedProjectId, setselectedProjectId] = useState(projidinbox)
  const [selectedLabel, setSelectedLabel] = useState([])

  const updateTaskFunc = async (content, due_date, due_string, description, projectId, labels) => {
    console.log(projectId, 'From update function');
    try {
      const response = await updateTaskapi(editmode.id,
        {
          content: content,
          due_date: due_date,
          due_string: due_string,
          description: description,
          projectId: projectId,
          labels: labels
        })
      dispatch(updateTaskAction(response))
      toggleform()
    }
    catch (error) {
      console.log('Error updating task', error);
    }
  }

  const addTask = async (name, date, string, description, projectId, labels) => {
    console.log(labels, "From Add function");
    console.log(projectId, ' id from add function');
    try {
      const response = await createTask(name, projectId, date, string, todaydate, description, labels)
      dispatch(createTaskAction(response))
    }
    catch (error) {
      console.log(error)
    }
  }

  const handleSubmit = () => {
    if (editmode) {
      updateTaskFunc(input, selecteddate, selectedstring, description, selectedProjectId, selectedLabel)
    }
    else {
      addTask(input, selecteddate, selectedstring, description, selectedProjectId, selectedLabel);
    }
    setinput('')
    setselecteddate(null)
    setDescription('')
    toggleform()
  }
  const onChange = (date, dateString) => {
    const str = String(date?.$d)
    const string = str.slice(4, 10)
    setselecteddate(dateString, string)
    setselectedstring(string)
  };
  useEffect(() => {
    if (editmode?.content && editmode?.due.date && editmode?.labels && editmode?.projectId) {
      setinput(editmode.content)
      setselecteddate(editmode.due.date)
      setselectedProjectId(editmode.projectId)
      setDescription(editmode.description)
      setSelectedLabel(editmode.labels)
    }
    else {
      setinput('')
      setDescription('')
      setselectedstring('');
    }
  }, [editmode]);
  return (
    <ConfigProvider
      button={{
        style: {
          width: 100,
          margin: 4,
        },
      }}
    >
      {isformopen ?
        (
          < Form onFinish={handleSubmit} className='form' style={{ display: 'flex', width: 770, flexDirection: 'column', height: '12rem', justifyContent: 'space-between', padding: '5px', marginLeft: '40px', borderRadius: '7px', marginTop: 10 }}>
            {editmode ? (
              <Form.Item  >
                <Input placeholder='Name' name="name" value={input} onChange={(e) => setinput(e.target.value)} style={{ height: '2rem', fontSize: '15px' }} />
              </Form.Item>
            ) : (
              <Form.Item name="name" rules={[{ required: true, message: 'Please Enter Task!' }]}>
                <Input placeholder='Name' value={input} onChange={(e) => setinput(e.target.value)} style={{ height: '2rem', fontSize: '15px' }} />
              </Form.Item>
            )}

            <Form.Item  >
              <Input.TextArea placeholder='Description' style={{ height: '2rem', border: 'none', marginBottom: '20px' }} value={description} onChange={(e) => setDescription(e.target.value)} />
            </Form.Item>

            <Form.Item style={{ marginTop: '-40px' }}>
              <DatePicker onChange={onChange} />
              <Select
                placeholder="Select Label"
                style={{ width: 200, marginLeft: 10 }}
                onChange={(value) => setSelectedLabel([value])}
              >
                {Labeldata.map((data) => (
                  <Option key={data.name} value={data.name}>{data.name}</Option>
                ))}
              </Select>
            </Form.Item>
            <Form.Item >
              <Select
                placeholder="Select Project"
                style={{ width: 150, marginRight: 360 }}
                onChange={(value) => setselectedProjectId(value)}
              >
                {projectnames.map((project) => (
                  <Option key={project.id} value={project.id}>{project.name}</Option>
                ))}
              </Select>
              <Space>
                <Button className='cancelbutton' onClick={toggleform}>Cancel</Button>
                <button danger htmlType="submit" className='addbutton'>{editmode ? "Save" : "Add Task"}</button>
              </Space>
            </Form.Item>
          </Form>
        )
        : (<div
          style={{
            clear: 'both',
            whiteSpace: 'nowrap',
          }}
        >
          <button className='Addformbutton' onClick={() => toggleform()} style={{ marginLeft: "30px", display: 'flex', border: 'none' }}><PlusOutlined style={{ marginRight: 5 }} />{title}</button>
        </div>)}
    </ConfigProvider >
  );
};
export default TaskForm;