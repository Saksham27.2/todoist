import React, { useState, useEffect } from 'react'
import { Typography } from 'antd'
import { useDispatch, useSelector } from 'react-redux';
import { deleteTask, getTasks, createTask, completeTask, updateTaskapi, unCompleteTask } from '../../Api/Api'
import { deleteTaskAction, fetchTaskAction, createTaskAction, updateTaskAction, checkboxTaskTodo, unCheckTaskTodo } from '../../Store/Features/TodosSlice'
import { useParams } from 'react-router-dom'
import Sections from '../Sections/Sections';
import { LoadingOutlined } from '@ant-design/icons';
import { Spin } from 'antd';
import TaskForm from './Form'
const { Title } = Typography;
import { useSnackbar } from 'notistack';
import './task.css'
import Taskdata from './TodayTask';

function Tasks() {

    const { enqueueSnackbar } = useSnackbar();
    const params = useParams()
    const prop = params.projectId
    let [projectId, projectName] = prop ? prop.split('-') : ''
    const dispatch = useDispatch()
    // const [projectid, setprojectid] = useState(projectId)
    const [editdata, seteditdata] = useState(null)
    const [loading, setLoading] = useState(true);
    const [isformopen, setisformopen] = useState(false)
    const [seletedTaskId, setSelectedTaskId] = useState(null)
    const [openForm, setOpenForm] = useState(false)
    const task = useSelector(state => state.todos.tasks)
    const activeTasks = task.filter(task => !task.isCompleted);

    const toggleform = () => {
        setisformopen(() => !isformopen)
    }
    const toggleForm = () => {
        setOpenForm(() => !openForm)
        seteditdata(null)
        setSelectedTaskId(null)
    }
    const fetchTask = async () => {
        try {
            const response = await getTasks()
            dispatch(fetchTaskAction(response))
            setLoading(false);
            // enqueueSnackbar("Fetched tasks", { variant: 'success' })
        }
        catch (err) {
            enqueueSnackbar('Failed in fetch task: ', err, { variant: 'error' })
        }
    }
    useEffect(() => {
        fetchTask()
    }, [])
    const handleEdit = (taskid) => {
        const dataset = task.filter(element => element.id === taskid)
        const data = dataset[0]
        seteditdata(data)
        setSelectedTaskId(taskid)
    }
    const today = new Date()
    const year = today.getFullYear()
    const month = today.getMonth() + 1
    const monthname = today.toLocaleString('default', { month: 'long' });
    const day = today.getDate()
    let todaydate = `${year}-${month < 10 ? '0' + month : month}-${day < 10 ? '0' + day : day}`
    const formatteddate = `${day} ${monthname}`


    const todaydata = activeTasks.filter((element) => element?.due?.date === todaydate)
    const tasklist = activeTasks.filter(task => (task?.projectId === projectId) && (task?.sectionId === null))

    return (
        <div style={{ display: 'flex', flexDirection: 'column', marginTop: '-40px' }}>
            {
                projectId ? <Title level={2} style={{ marginLeft: "25px" }}> {projectName}</Title> : <Title level={3} style={{ marginLeft: "25px" }}>{formatteddate} Today</Title>
            }
            <center><Spin spinning={loading} indicator={<LoadingOutlined style={{ fontSize: 24 }} spin />} /></center>
            {
                projectName ?
                    tasklist.map((data) => (
                        <div key={data.id} style={{ width: 850 }}>
                            {
                                seletedTaskId === data.id ?
                                    (<TaskForm editmode={editdata} isformopen={openForm} todaydate={todaydate} toggleform={toggleForm} />)
                                    :
                                    (<Taskdata data={data} title={'taskdata'} handleedit={handleEdit} todaydate={todaydate} setOpenForm={setOpenForm} />)
                            }
                        </div>
                    ))
                    :
                    todaydata.map((data) => (
                        <div key={data.id}>
                            {
                                seletedTaskId === data.id ?
                                    (<TaskForm editmode={editdata} isformopen={openForm} toggleform={toggleForm} />)
                                    :
                                    (<Taskdata data={data} title={'todaydata'} handleedit={handleEdit} todaydate={todaydate} setOpenForm={setOpenForm} />)
                            }
                        </div>
                    ))
            }
            <TaskForm title={"Add Task"} h isformopen={isformopen} toggleform={toggleform} />
            {
                projectId && <Sections editmode={editdata} projectId={projectId} tasks={task}  handleedit={handleEdit} setOpenForm={setOpenForm} />
            }
        </div>
    )
}
export default Tasks
